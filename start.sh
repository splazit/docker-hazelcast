if [ -e "$HZ_CONFIG" ]
then
   java -Dhazelcast.config=$HZ_CONFIG -server -cp hazelcast-$HZ_VERSION.jar com.hazelcast.core.server.StartServer
else
   java -server -cp hazelcast-$HZ_VERSION.jar com.hazelcast.core.server.StartServer
fi
